# Evaluator Starter

All files needed to start evaluator application.

## Description
Evaluator MS is a bunch of microservices created with help of [Spring Cloud](https://spring.io/projects/spring-cloud) for calculating cost of travel or seeing statistics about specific model. 

<p align="center">
    <img align="center" src="images/evaluator-diagram.png">
</p>

Example use:

<p align="center">
    <img align="center" src="images/route-sample.png" width="1000" height="600" >
</p>

### [Evaluator User Interface](https://gitlab.com/evaluator-ms/evaluator-user-interface)
Evaluator UI simple web UI which uses Gateway Service to communicates with backend services.

### [Car Scraper](https://gitlab.com/evaluator-ms/car-scraper)
Car Scraper is application for extracting vehicles data from [Autocentrum web site](https://www.autocentrum.pl/). With help of [jsoup library](https://jsoup.org/) it finds details about cars, such as models, generations, version, engines. It also finds fuel consumption and cars rates based on vehicle users reports and opinions.

### [Price Evaluator](https://gitlab.com/evaluator-ms/price-evaluator)
Web scraping based service which is used to find statistics about offers on chosen vehicle model. It takes advantage of [Otomoto service](https://www.otomoto.pl/). Price Evaluator communicates with Car Scraper through RabbitMQ message broker for findind details about vehicles.

### [Route Coster](https://gitlab.com/evaluator-ms/route-coster)
Service for evaluating travel cost. Based on [Google Distance Matrix API](https://developers.google.com/maps/documentation/distance-matrix/start) it finds distance and travel time beetwen appointed addresses. To find specific car fuel consumption Route Coster communicate through RabbitMQ with Car Scraper.

### Spring Cloud Services
[Eureka Server](https://gitlab.com/evaluator-ms/eureka-server), [Gateway Service](https://gitlab.com/evaluator-ms/gateway-service) and [Config Service](https://gitlab.com/evaluator-ms/config-service) are Spring Cloud components for service discover, API Gateway and external configuration, respectively.

### RabbitMQ
RabbitMQ is an open source message broker. You can learn more about RabbitMQ [here](https://www.rabbitmq.com/getstarted.html).

### MongoDB
MongoDb is noSQL database to store JSON-like documents. You can learn more about Mongo [here](https://www.mongodb.com/what-is-mongodb).

## Launching 
In order to launch [Evaluator MS](https://gitlab.com/evaluator-ms) application clone this repo and run this command in project root directory:

`docker-compose up -d`

Images are downloading now from *registry.gitlab.com*. It may takes a couple of minutes. After images are downloaded, containers are launched.
Wait till all containers are up (`docker ps`) and go to Eureka Dashboard on [http://localhost:8761/](http://localhost:8761/). You should see

![](images/eureka.png)

If not all 6 applications are there wait another moment.

When all services are ready go to [http://localhost:9000/route](http://localhost:9000/route) 
pick your car, origin and destination addresses and evaluate cost of your travel.

## Cloning
You may clone all services repositories using *clone.sh* file. Just go to terminal
and run:

`sh clone.sh`


 