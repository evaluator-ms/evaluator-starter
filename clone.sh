#!/bin/bash
echo 'Starting cloning Evaluator MS repositories...'
mkdir evaluator-ms
cd evaluator-ms
git clone https://gitlab.com/evaluator-ms/config-service.git
git clone https://gitlab.com/evaluator-ms/eureka-server.git
git clone https://gitlab.com/evaluator-ms/evaluator-user-interface.git
git clone https://gitlab.com/evaluator-ms/gateway-service.git
git clone https://gitlab.com/evaluator-ms/route-coster.git
git clone https://gitlab.com/evaluator-ms/car-scraper.git
git clone https://gitlab.com/evaluator-ms/price-evaluator.git
echo 'Cloned repositories into /evaluator-ms'
